package main

import (
	"context"
	"log"
	"os"
	"strconv"

	"github.com/barrydevp/codeatest-runner-core/dispatcher"
	"github.com/barrydevp/codeatest-runner-core/puller"
	"github.com/barrydevp/codeatest-runner-core/runner"
	"github.com/barrydevp/codeatest-runner-core/server"
)

const DEFAULT_PORT = "5004"

var RUNNER_NAME string
var PORT string
var BUCKET_SIZE int64

func init() {
	envPORT := os.Getenv("PORT")

	if envPORT == "" {
		PORT = DEFAULT_PORT
		log.Printf("DEFAULT_PORT %s is set.\n", DEFAULT_PORT)
	} else {
		PORT = envPORT
	}

	RUNNER_NAME = os.Getenv("RUNNER_NAME")
	if RUNNER_NAME == "" {
		RUNNER_NAME = "G++9"
	}

	bucketSize := os.Getenv("CONCURRENCY")

	if bucketSize == "" {
		BUCKET_SIZE = 1
	} else {
		vBucketSize, err := strconv.ParseInt(bucketSize, 10, 64)

		if err != nil {
			log.Println("parse BUCKET_SIZE error, using default BUCKETSIZE = 1")
			BUCKET_SIZE = 1
		} else {
			BUCKET_SIZE = vBucketSize
		}
	}
}

func main() {
	log.Printf("[RUNNER::%s] UP AND RUNNING...\n", RUNNER_NAME)

	ctx := context.Background()

	CPPRunner := runner.Runner{
		Name:  RUNNER_NAME,
		State: "created",

		NeedBuild:     true,
		BuildCommand:  "g++",
		BuildBaseArgs: []string{"-o"},
		Command:       "",
		BaseArgs:      []string{},
	}

	CPPPuller := puller.Puller{
		Language:   "cpp",
		BucketSize: BUCKET_SIZE,
	}

	CPPDispatcher := dispatcher.Dispatcher{
		Name:      RUNNER_NAME,
		Runner:    &CPPRunner,
		Puller:    &CPPPuller,
		Ctx:       ctx,
		IsRunning: false,
		Delay:     10,
	}

	CPPDispatcher.Init()

	go CPPDispatcher.Run()

	app := server.HttpServer{Dispatcher: &CPPDispatcher, PORT: PORT}

	app.ListenAndServe()

	log.Printf("[RUNNER::%s] DOWN BYE BYE...\n", RUNNER_NAME)
}
